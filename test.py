#!/usr/bin/python3

# checker needs to be compiled in debug mode for this to work
# usage:  ./test.py <range>

import sys

try:
    nrange = sys.argv[1]
except IndexError:
    nrange = 100

ntests = 50

import os

cmd = 'ARG=`./randnumbers.py %s`; ./push_swap $ARG | ./checker $ARG' % nrange
results = []
for i in range(ntests):
    x = os.popen(cmd)
    results.append(int(x.read().strip()))
total = 0
for i in results:
    total += i
print(total / (ntests * 1.0))
