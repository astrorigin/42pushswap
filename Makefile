# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/11/08 22:24:40 by pmarquis          #+#    #+#              #
#    Updated: 2022/12/26 13:27:22 by pmarquis         ###   lausanne.ch        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re bonus mrproper

# options

OPT_DEBUG = 0
export OPT_DEBUG

OPT_LINUX = 0
export OPT_LINUX

# end options

NAME = push_swap
NAME_B = checker

CC = gcc

ifeq ($(OPT_DEBUG),1)
CFLAGS = -Wall -Wextra -g -fsanitize=address
else
CFLAGS = -Wall -Wextra -Werror
endif

DEFINES =
ifeq ($(OPT_DEBUG),0)
DEFINES += -DNDEBUG
endif

# push_swap
INC = pushswap.h
INC := $(addprefix src/,$(INC))
SRC = align_a.c \
	  compar_int.c \
	  direction.c \
	  dist_top_rev_rotate.c \
	  dist_top_rotate.c \
	  do_push.c \
	  do_rev_rotate.c \
	  do_rev_rotate_both.c \
	  do_rev_rotate_x.c \
	  do_rotate.c \
	  do_rotate_both.c \
	  do_rotate_x.c \
	  do_swap.c \
	  error.c \
	  index_stak.c \
	  init.c \
	  main.c \
	  mark.c \
	  mark_by_greater.c \
	  mark_by_index.c \
	  move.c \
	  other.c \
	  print_cmds.c \
	  print_op.c \
	  solve.c \
	  solve_a.c \
	  solve_b.c \
	  sort_2.c \
	  sort_3.c \
	  sort_4.c \
	  sort_5.c \
	  sort_6.c \
	  sort_7.c \
	  stak_add.c \
	  stak_find_max.c \
	  stak_find_min.c \
	  stak_find_num.c \
	  stak_free.c \
	  stak_is_inverted.c \
	  stak_is_sorted.c \
	  stak_next.c \
	  stak_prev.c
SRC := $(addprefix src/,$(SRC))
OBJ = $(patsubst %.c,%.o,$(SRC))
INCLUDES = -Ilibft -Ilibft/arr -Ilibft/printf -Ilibft/gnl
ARCHIVES = libft/arr/libftarr.a libft/printf/libftprintf.a libft/libft.a

# checker
INC_B = pushswap.h
INC_B := $(addprefix src/,$(INC_B))
SRC_B = checker.c compar_int.c do_push.c do_rev_rotate.c do_rev_rotate_both.c \
		do_rev_rotate_x.c do_rotate_both.c do_rotate.c do_rotate_x.c do_swap.c \
		error.c exec.c final_check.c init.c print_op.c stak_add.c \
		stak_find_max.c stak_find_min.c stak_find_num.c stak_is_sorted.c
SRC_B := $(addprefix src/,$(SRC_B))
OBJ_B = $(patsubst %.c,%.o,$(SRC_B))
ARCHIVES_B = libft/arr/libftarr.a libft/printf/libftprintf.a \
			 libft/gnl/libftgnl.a libft/libft.a

.c.o:
	$(CC) -c $(CFLAGS) $(DEFINES) $(INCLUDES) -o $@ $<

.DEFAULT_GOAL = all

all: $(NAME)

$(NAME): $(OBJ) $(ARCHIVES)
	$(CC) $(CFLAGS) $^ -o $@

$(NAME_B): $(OBJ_B) $(ARCHIVES_B)
	$(CC) $(CFLAGS) $^ -o $@

libft/libft.a:
	$(MAKE) -C libft

libft/arr/libftarr.a:
	$(MAKE) -C libft/arr

libft/gnl/libftgnl.a:
	$(MAKE) -C libft/gnl

libft/printf/libftprintf.a:
	$(MAKE) -C libft/printf

clean:
	rm -f src/*.o
	$(MAKE) -C libft $@

fclean: clean
	rm -f $(NAME) $(NAME_B)
	$(MAKE) -C libft $@

re: fclean all

bonus: $(NAME_B)

mrproper: fclean
	rm -f cscope.* tags
	$(MAKE) -C libft $@

### deps
$(OBJ): $(INC)
$(OBJ_B): $(INC)
