#!/usr/bin/python3

# usage:  ARG=`./randnumbers.py <NUM>`; ./push_swap $ARG | ./checker $ARG

import sys
import random

x = int(sys.argv[1])
random.seed()
seq = [str(i) for i in range(x)]
random.shuffle(seq)
print(' '.join(seq))
