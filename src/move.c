/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/14 01:50:17 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 23:13:57 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

// for cases: 4 or 5 numbers
void	put_min_to_top(t_stak *stak, char which)
{
	const t_node	*nd = stak_find_min(stak);

	if (nd == stak->bottom)
		pr_rev_rotate(stak, which);
	else if (nd == stak->top->next)
		pr_swap(stak, which);
	else if (nd == stak->bottom->prev)
		pr_rev_rotate_x_times(stak, 2, which);
	else if (nd != stak->top)
		pr_rotate_x_times(stak, 2, which);
}

// for cases: 6 or 7 numbers
void	move_to_top(t_stak *stak, t_node *nd, char which)
{
	int	i;
	int	j;

	i = dist_top_rotate(stak, nd);
	if (!i)
		return ;
	j = dist_top_rev_rotate(stak, nd);
	if (i <= j)
		pr_rotate_x_times(stak, i, which);
	else
		pr_rev_rotate_x_times(stak, j, which);
}
