/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_rotate.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/09 19:06:15 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 23:11:27 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

void	do_rotate(t_stak *stak)
{
	t_node	*nd;

	if (stak->cnt < 2)
		return ;
	nd = stak->top;
	ft_dllist_take(&stak->top, nd, 0);
	ft_dllist_pushback(&stak->top, nd, stak->bottom);
	stak->bottom = nd;
}

void	pr_rotate(t_stak *stak, char which)
{
	do_rotate(stak);
	print_op("r", which);
}

int	add_rotate(t_stak *stak, char which, t_arr *cmds)
{
	do_rotate(stak);
	if (!add_op("r", which, cmds))
		return (0);
	return (1);
}
