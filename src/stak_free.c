/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stak_free.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/24 01:35:13 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/24 01:37:45 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

void	stak_free(t_stak *stak)
{
	t_node	*nd;
	t_node	*next;

	nd = stak->top;
	while (nd)
	{
		next = nd->next;
		ft_free(nd);
		nd = next;
	}
	ft_memset(stak, 0, sizeof(t_stak));
}
