/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   other.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/15 16:13:22 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/20 03:47:07 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

char	other(char which)
{
	if (which == 'a')
		return ('b');
	else if (which == 'b')
		return ('a');
	assert(0);
	return (0);
}
