/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_rev_rotate_both.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/25 23:06:42 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 23:06:52 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

void	do_rev_rotate_both(t_stak *a, t_stak *b)
{
	do_rev_rotate(a);
	do_rev_rotate(b);
}

void	pr_rev_rotate_both(t_stak *a, t_stak *b)
{
	do_rev_rotate_both(a, b);
	print_op("rr", 'r');
}

int	add_rev_rotate_both(t_stak *a, t_stak *b, t_arr *cmds)
{
	do_rev_rotate_both(a, b);
	if (!add_op("rr", 'r', cmds))
		return (0);
	return (1);
}
