/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_push.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/09 18:46:27 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/24 11:04:42 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

void	do_push(t_stak *from, t_stak *to)
{
	t_node	*nd;

	if (!from->cnt)
		return ;
	nd = from->top;
	ft_dllist_take(&from->top, nd, 0);
	if (from->cnt == 1)
		from->bottom = 0;
	--(from->cnt);
	ft_dllist_push(&to->top, nd);
	if (!to->cnt)
		to->bottom = nd;
	++(to->cnt);
}

void	pr_push(t_stak *from, t_stak *to, char which)
{
	do_push(from, to);
	print_op("p", which);
}

int	add_push(t_stak *from, t_stak *to, char which, t_arr *cmds)
{
	do_push(from, to);
	if (!add_op("p", which, cmds))
		return (0);
	return (1);
}
