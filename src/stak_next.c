/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stak_next.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/24 18:30:20 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/24 18:34:13 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

t_node	*stak_next(const t_stak *stak, const t_node *nd)
{
	t_node	*next;

	next = nd->next;
	if (!next)
		return (stak->top);
	return (next);
}
