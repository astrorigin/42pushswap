/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mark_by_index.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/23 21:10:21 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 17:06:46 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

size_t	mark_by_index(t_stak *stak, t_node *nd)
{
	int		i;
	size_t	keep_cnt;
	t_node	*n;

	i = nd->index;
	nd->keep = 1;
	n = stak_next(stak, nd);
	keep_cnt = 1;
	while (n != nd)
	{
		if (n->index == i + 1)
		{
			n->keep = 1;
			++i;
			++keep_cnt;
		}
		else
			n->keep = 0;
		n = stak_next(stak, n);
	}
	return (keep_cnt);
}
