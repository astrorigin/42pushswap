/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stak_find_num.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/20 03:27:07 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/20 03:27:21 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

t_node	*stak_find_num(const t_stak *stak, int num)
{
	t_node	*nd;

	nd = stak->top;
	while (nd)
	{
		if (nd->num == num)
			return (nd);
		nd = nd->next;
	}
	return (0);
}
