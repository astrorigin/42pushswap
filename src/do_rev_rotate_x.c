/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_rev_rotate_x.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/14 23:31:54 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/14 23:35:38 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

void	do_rev_rotate_x_times(t_stak *stak, size_t x)
{
	size_t	i;

	i = 0;
	while (i++ < x)
		do_rev_rotate(stak);
}

void	pr_rev_rotate_x_times(t_stak *stak, size_t x, char which)
{
	size_t	i;

	i = 0;
	while (i++ < x)
		pr_rev_rotate(stak, which);
}

void	do_rev_rotate_both_x_times(t_stak *a, t_stak *b, size_t x)
{
	size_t	i;

	i = 0;
	while (i++ < x)
		do_rev_rotate_both(a, b);
}

void	pr_rev_rotate_both_x_times(t_stak *a, t_stak *b, size_t x)
{
	size_t	i;

	i = 0;
	while (i++ < x)
		pr_rev_rotate_both(a, b);
}
