/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/24 01:30:59 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 23:15:47 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

int	solve(t_parms *parms, size_t (*marker)(t_stak *, t_node *), t_arr *cmds)
{
	if (!solve_a(parms, marker, cmds))
		return (0);
	if (!solve_b(parms, cmds))
		return (0);
	align_a(&parms->a, cmds);
	stak_free(&parms->a);
	stak_free(&parms->b);
	return (1);
}
