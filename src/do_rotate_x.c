/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_rotate_x.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/14 20:04:34 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/14 23:25:30 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

void	do_rotate_x_times(t_stak *stak, size_t x)
{
	size_t	i;

	i = 0;
	while (i++ < x)
		do_rotate(stak);
}

void	pr_rotate_x_times(t_stak *stak, size_t x, char which)
{
	size_t	i;

	i = 0;
	while (i++ < x)
		pr_rotate(stak, which);
}

void	do_rotate_both_x_times(t_stak *a, t_stak *b, size_t x)
{
	size_t	i;

	i = 0;
	while (i++ < x)
		do_rotate_both(a, b);
}

void	pr_rotate_both_x_times(t_stak *a, t_stak *b, size_t x)
{
	size_t	i;

	i = 0;
	while (i++ < x)
		pr_rotate_both(a, b);
}
