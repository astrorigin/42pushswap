/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dist_top_rotate.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/20 03:39:45 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/20 22:29:53 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

// count rotates to go to top
// return int >= 0

int	dist_top_rotate(const t_stak *stak, const t_node *nd)
{
	int		i;
	t_node	*n;

	i = 0;
	n = stak->top;
	while (n)
	{
		if (n == nd)
			return (i);
		n = n->next;
		++i;
	}
	assert(0);
	return (INT_MAX);
}
