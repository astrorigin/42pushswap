/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_5.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/09 23:38:40 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/15 16:25:52 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

void	sort_5(t_stak *a, t_stak *b, char which)
{
	put_min_to_top(a, which);
	if (stak_is_sorted(a))
		return ;
	pr_push(a, b, other(which));
	put_min_to_top(a, which);
	if (stak_is_sorted(a))
	{
		pr_push(b, a, which);
		return ;
	}
	pr_push(a, b, other(which));
	if (!stak_is_sorted(a))
		sort_3(a, which);
	pr_push(b, a, which);
	pr_push(b, a, which);
}
