/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stak_is_inverted.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/20 03:45:36 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/20 03:45:51 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

int	stak_is_inverted(const t_stak *stak)
{
	t_node	*nd;
	int		num;

	nd = stak->top;
	num = nd->num;
	nd = nd->next;
	while (nd)
	{
		if (num <= nd->num)
			return (0);
		num = nd->num;
		nd = nd->next;
	}
	return (1);
}
