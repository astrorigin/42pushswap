/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/09 19:56:27 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 14:58:53 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

static int	init_onearg(t_stak *stak, char *s)
{
	long long	ll;

	while (*s)
	{
		s = ft_atoll(s, &ll);
		if (!s || ll > INT_MAX || ll < INT_MIN)
			return (0);
		if (stak_find_num(stak, (int) ll) || !stak_add(stak, (int) ll))
			return (0);
	}
	return (1);
}

static int	init_manyarg(t_stak *stak, int argc, char *argv[])
{
	int			i;
	long long	ll;
	char		*p;

	i = 0;
	while (i < argc)
	{
		p = ft_atoll(argv[i], &ll);
		if (!p || *p || ll > INT_MAX || ll < INT_MIN)
			return (0);
		if (stak_find_num(stak, (int) ll) || !stak_add(stak, (int) ll))
			return (0);
		++i;
	}
	return (1);
}

int	init(t_stak *a, int argc, char **argv)
{
	if (argc == 2 && ft_strchr(argv[1], ' '))
	{
		if (!init_onearg(a, argv[1]))
			return (error(1));
	}
	else
	{
		if (!init_manyarg(a, argc - 1, &argv[1]))
			return (error(1));
	}
	return (1);
}

int	init_cmds(t_arr *cmds_in, t_arr *cmds_gt)
{
	if (!ft_arr_init(cmds_in, 1000, sizeof(char *))
		|| !ft_arr_init(cmds_gt, 1000, sizeof(char *)))
		return (0);
	return (1);
}
