/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   direction.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/24 17:50:29 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 23:05:27 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

static t_node	*find_a_node(t_stak *a, int index)
{
	t_node	*nd;

	nd = a->mark;
	if (index < nd->index)
	{
		while (index < (stak_prev(a, nd))->index
			&& nd->index > (stak_prev(a, nd))->index)
			nd = stak_prev(a, nd);
	}
	else
	{
		while (index > nd->index && nd->index < (stak_next(a, nd))->index)
			nd = stak_next(a, nd);
		if (index > nd->index && nd->index > (stak_next(a, nd))->index)
			nd = stak_next(a, nd);
	}
	return (nd);
}

// find shortest way to bring nd with 'index' on top
static void	estimate_dir(t_stak *stak, int index, size_t *rx_sz, size_t *rrx_sz)
{
	t_node	*nd;

	nd = stak->top;
	while (nd->index != index)
	{
		++(*rx_sz);
		nd = stak_next(stak, nd);
	}
	nd = stak->top;
	while (nd->index != index)
	{
		++(*rrx_sz);
		nd = stak_prev(stak, nd);
	}
}

static void	set_dir(size_t size, const t_shift *new_sh, t_shift *sh)
{
	if (!sh->is_set || size < sh->sz)
	{
		sh->a_nd = new_sh->a_nd;
		sh->b_nd = new_sh->b_nd;
		sh->a_dir = new_sh->a_dir;
		sh->b_dir = new_sh->b_dir;
		sh->sz = size;
		sh->is_set = 1;
	}
}

static void	optimal_dir(t_stak *a, t_stak *b, t_node *b_nd, t_shift *sh)
{
	t_shift	new_sh;
	size_t	ra;
	size_t	rra;
	size_t	rb;
	size_t	rrb;

	ft_memset(&new_sh, 0, sizeof(t_shift));
	ra = 0;
	rra = 0;
	rb = 0;
	rrb = 0;
	new_sh.a_nd = find_a_node(a, b_nd->index);
	new_sh.b_nd = b_nd;
	estimate_dir(a, new_sh.a_nd->index, &ra, &rra);
	estimate_dir(b, b_nd->index, &rb, &rrb);
	new_sh.a_dir = 0;
	new_sh.b_dir = 0;
	set_dir(ft_ulmax(ra, rb), &new_sh, sh);
	new_sh.a_dir = 1;
	set_dir(rra + rb, &new_sh, sh);
	new_sh.b_dir = 1;
	set_dir(ft_ulmax(rra, rrb), &new_sh, sh);
	new_sh.a_dir = 0;
	set_dir(ra + rrb, &new_sh, sh);
}

void	opt_direction(t_stak *a, t_stak *b, t_shift *sh)
{
	size_t	i;
	t_node	*nd;

	nd = b->top;
	i = -1;
	while (++i < b->cnt)
	{
		optimal_dir(a, b, nd, sh);
		nd = stak_next(b, nd);
	}
}
