/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_3.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/09 23:38:40 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/15 16:03:03 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

void	sort_3(t_stak *stak, char which)
{
	const t_node	*lo = stak_find_min(stak);
	const t_node	*hi = stak_find_max(stak);

	if (lo == stak->top)
	{
		pr_swap(stak, which);
		pr_rotate(stak, which);
	}
	else if (lo == stak->bottom)
	{
		if (hi == stak->top)
		{
			pr_swap(stak, which);
			pr_rev_rotate(stak, which);
		}
		else
			pr_rev_rotate(stak, which);
	}
	else
	{
		if (hi == stak->top)
			pr_rotate(stak, which);
		else
			pr_swap(stak, which);
	}
}
