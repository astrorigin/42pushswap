/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stak_add.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/09 16:59:21 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/15 00:08:54 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

static t_node	*node_new(int num)
{
	t_node	*nd;

	nd = ft_calloc(1, sizeof(t_node));
	if (!nd)
		return (0);
	nd->num = num;
	return (nd);
}

int	stak_add(t_stak *stak, int num)
{
	t_node	*nd;

	nd = node_new(num);
	if (!nd)
		return (0);
	ft_dllist_pushback(&stak->top, nd, stak->bottom);
	stak->bottom = nd;
	++(stak->cnt);
	return (1);
}
