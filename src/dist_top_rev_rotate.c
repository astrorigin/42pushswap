/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dist_top_rev_rotate.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/12 23:47:19 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/20 22:30:07 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

// count rev-rotates to go to top
// return int >= 1

int	dist_top_rev_rotate(const t_stak *stak, const t_node *nd)
{
	int		i;
	t_node	*n;

	i = 1;
	n = stak->bottom;
	while (n)
	{
		if (n == nd)
			return (i);
		n = n->prev;
		++i;
	}
	assert(0);
	return (INT_MIN);
}
