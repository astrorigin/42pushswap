/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_rev_rotate.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/09 19:18:23 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 23:11:27 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

void	do_rev_rotate(t_stak *stak)
{
	t_node	*nd;
	t_node	*prev;

	if (stak->cnt < 2)
		return ;
	nd = stak->bottom;
	prev = nd->prev;
	ft_dllist_take(&stak->top, nd, prev);
	stak->bottom = prev;
	ft_dllist_push(&stak->top, nd);
}

void	pr_rev_rotate(t_stak *stak, char which)
{
	do_rev_rotate(stak);
	print_op("rr", which);
}

int	add_rev_rotate(t_stak *stak, char which, t_arr *cmds)
{
	do_rev_rotate(stak);
	if (!add_op("rr", which, cmds))
		return (0);
	return (1);
}
