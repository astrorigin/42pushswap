/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/09 21:16:55 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/15 00:01:30 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

int	exec_s(const char *op, t_stak *a, t_stak *b)
{
	if (!ft_strcmp(op, "sa\n"))
		do_swap(a);
	else if (!ft_strcmp(op, "sb\n"))
		do_swap(b);
	else if (!ft_strcmp(op, "ss\n"))
		do_swap_both(a, b);
	else
		return (0);
	return (1);
}

int	exec_p(const char *op, t_stak *a, t_stak *b)
{
	if (!ft_strcmp(op, "pa\n"))
		do_push(b, a);
	else if (!ft_strcmp(op, "pb\n"))
		do_push(a, b);
	else
		return (0);
	return (1);
}

int	exec_r(const char *op, t_stak *a, t_stak *b)
{
	if (!ft_strcmp(op, "ra\n"))
		do_rotate(a);
	else if (!ft_strcmp(op, "rb\n"))
		do_rotate(b);
	else if (!ft_strcmp(op, "rr\n"))
		do_rotate_both(a, b);
	else if (!ft_strcmp(op, "rra\n"))
		do_rev_rotate(a);
	else if (!ft_strcmp(op, "rrb\n"))
		do_rev_rotate(b);
	else if (!ft_strcmp(op, "rrr\n"))
		do_rev_rotate_both(a, b);
	else
		return (0);
	return (1);
}
