/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mark.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/24 01:24:36 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/24 18:45:06 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

void	mark(t_stak *stak, size_t (*marker)(t_stak *, t_node *))
{
	size_t	i;
	size_t	keep_cnt;
	t_node	*nd;

	nd = stak->top;
	i = -1;
	while (++i < stak->cnt)
	{
		keep_cnt = (*marker)(stak, nd);
		if (keep_cnt > stak->keep_cnt)
		{
			stak->mark = nd;
			stak->keep_cnt = keep_cnt;
		}
		else if (keep_cnt == stak->keep_cnt
			&& (!stak->mark || nd->num < stak->mark->num))
		{
			stak->mark = nd;
		}
		nd = stak_next(stak, nd);
	}
	(*marker)(stak, stak->mark);
}
