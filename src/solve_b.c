/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_b.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/24 11:30:19 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 23:27:21 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

// bring chosen nodes on top of stacks
static void	move_b(t_stak *a, t_stak *b, t_shift *sh, t_arr *cmds)
{
	while (sh->a_nd != a->top || sh->b_nd != b->top)
	{
		if (sh->a_dir == sh->b_dir && sh->a_nd != a->top && sh->b_nd != b->top)
		{
			if (sh->a_dir == 0)
				add_rotate_both(a, b, cmds);
			else
				add_rev_rotate_both(a, b, cmds);
		}
		else if (sh->a_nd != a->top)
		{
			if (sh->a_dir == 0)
				add_rotate(a, 'a', cmds);
			else
				add_rev_rotate(a, 'a', cmds);
		}
		else if (sh->b_nd != b->top)
		{
			if (sh->b_dir == 0)
				add_rotate(b, 'b', cmds);
			else
				add_rev_rotate(b, 'b', cmds);
		}
	}
	assert(sh->a_nd == a->top && sh->b_nd == b->top);
}

int	solve_b(t_parms *parms, t_arr *cmds)
{
	t_shift	sh;

	ft_memset(&sh, 0, sizeof(t_shift));
	while (parms->b.cnt)
	{
		sh.is_set = 0;
		opt_direction(&parms->a, &parms->b, &sh);
		move_b(&parms->a, &parms->b, &sh, cmds);
		add_push(&parms->b, &parms->a, 'a', cmds);
	}
	return (1);
}
