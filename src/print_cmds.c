/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_cmds.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/25 15:04:34 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 15:25:07 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

void	print_cmds(const t_arr *cmds)
{
	size_t	i;
	char	**s;

	i = 0;
	while (i < cmds->nelem)
	{
		s = ft_arr_get(cmds, i++);
		ft_putstr(*s, 1);
	}
}
