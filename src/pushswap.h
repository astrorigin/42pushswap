/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pushswap.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/08 22:23:35 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/26 00:06:34 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSHSWAP_H
# define PUSHSWAP_H

# ifndef NDEBUG
#  include <assert.h>
# endif

# include <libft.h>
# include <ft_arr.h>
# include <ft_printf.h>
# include <ft_getnext.h>

typedef struct s_node
{
	struct s_node	*next;
	struct s_node	*prev;
	int				num;
	int				index;
	int				keep;
}	t_node;

typedef struct s_stak
{
	t_node	*top;
	t_node	*bottom;
	size_t	cnt;
	t_node	*mark;
	size_t	keep_cnt;
}	t_stak;

typedef struct s_parms
{
	t_stak	a;
	t_stak	b;
	int		argc;
	char	**argv;
}	t_parms;

typedef struct s_shift
{
	t_node	*a_nd;
	t_node	*b_nd;
	int		a_dir;
	int		b_dir;
	size_t	sz;
	int		is_set;
}	t_shift;

int		error(int verbose);
char	other(char which);

int		init(t_stak *a, int argc, char **argv);
int		init_cmds(t_arr *cmds_in, t_arr *cmds_gt);
int		index_stak(t_stak *a);

void	print_op(const char *op, char suffix);
int		add_op(const char *op, char suffix, t_arr *cmds);
void	print_cmds(const t_arr *cmds);

void	do_push(t_stak *from, t_stak *to);
void	pr_push(t_stak *from, t_stak *to, char which);
int		add_push(t_stak *from, t_stak *to, char which, t_arr *cmds);

void	do_swap(t_stak *stak);
void	pr_swap(t_stak *stak, char which);
int		add_swap(t_stak *stak, char which, t_arr *cmds);

void	do_swap_both(t_stak *a, t_stak *b);
void	pr_swap_both(t_stak *a, t_stak *b);

void	do_rotate(t_stak *stak);
void	pr_rotate(t_stak *stak, char which);
int		add_rotate(t_stak *stak, char which, t_arr *cmds);

void	do_rotate_both(t_stak *a, t_stak *b);
void	pr_rotate_both(t_stak *a, t_stak *b);
int		add_rotate_both(t_stak *a, t_stak *b, t_arr *cmds);

void	do_rev_rotate(t_stak *stak);
void	pr_rev_rotate(t_stak *stak, char which);
int		add_rev_rotate(t_stak *stak, char which, t_arr *cmds);

void	do_rev_rotate_both(t_stak *a, t_stak *b);
void	pr_rev_rotate_both(t_stak *a, t_stak *b);
int		add_rev_rotate_both(t_stak *a, t_stak *b, t_arr *cmds);

void	do_rotate_x_times(t_stak *stak, size_t x);
void	pr_rotate_x_times(t_stak *stak, size_t x, char which);

void	do_rotate_both_x_times(t_stak *a, t_stak *b, size_t x);
void	pr_rotate_both_x_times(t_stak *a, t_stak *b, size_t x);

void	do_rev_rotate_x_times(t_stak *stak, size_t x);
void	pr_rev_rotate_x_times(t_stak *stak, size_t x, char which);

void	do_rev_rotate_both_x_times(t_stak *a, t_stak *b, size_t x);
void	pr_rev_rotate_both_x_times(t_stak *a, t_stak *b, size_t x);

int		compar_int(int *a, int *b);

int		stak_add(t_stak *stak, int num);
void	stak_free(t_stak *stak);

t_node	*stak_next(const t_stak *stak, const t_node *nd);
t_node	*stak_prev(const t_stak *stak, const t_node *nd);

t_node	*stak_find_max(const t_stak *stak);
t_node	*stak_find_min(const t_stak *stak);
t_node	*stak_find_num(const t_stak *stak, int num);

int		stak_is_sorted(const t_stak *stak);
int		stak_is_inverted(const t_stak *stak);

void	put_min_to_top(t_stak *stak, char which);

void	sort_2(t_stak *stak, char which);
void	sort_3(t_stak *stak, char which);
void	sort_4(t_stak *a, t_stak *b, char which);
void	sort_5(t_stak *a, t_stak *b, char which);
void	sort_6(t_stak *a, t_stak *b, char which);
void	sort_7(t_stak *a, t_stak *b, char which);

int		dist_top_rotate(const t_stak *stak, const t_node *nd);
int		dist_top_rev_rotate(const t_stak *stak, const t_node *nd);

void	move_to_top(t_stak *stak, t_node *nd, char which);

void	mark(t_stak *stak, size_t (*marker)(t_stak *, t_node *));
size_t	mark_by_index(t_stak *stak, t_node *nd);
size_t	mark_by_greater(t_stak *stak, t_node *nd);

int		solve(t_parms *parms, size_t (*marker)(t_stak *, t_node *),
			t_arr *cmds);
int		solve_a(t_parms *parms, size_t (*marker)(t_stak *, t_node *),
			t_arr *cmds);
int		solve_b(t_parms *parms, t_arr *cmds);
void	opt_direction(t_stak *a, t_stak *b, t_shift *sh);
void	align_a(t_stak *a, t_arr *cmds);

# ifndef NDEBUG

void	stak_debug(const t_stak *stak);
void	shift_debug(const t_shift *sh);

# endif

// bonus part

int		exec_s(const char *op, t_stak *a, t_stak *b);
int		exec_p(const char *op, t_stak *a, t_stak *b);
int		exec_r(const char *op, t_stak *a, t_stak *b);

int		final_check(const t_stak *a, const t_stak *b, size_t cnt);

#endif
