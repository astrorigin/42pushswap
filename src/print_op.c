/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_op.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/10 14:35:48 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 15:18:10 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

void	print_op(const char *op, char suffix)
{
	char	buf[5];
	char	end[3];

	ft_strcpy(buf, op);
	end[0] = suffix;
	end[1] = '\n';
	end[2] = 0;
	ft_strcat(buf, end);
	ft_putstr(buf, 1);
}

int	add_op(const char *op, char suffix, t_arr *cmds)
{
	char	buf[5];
	char	end[3];
	char	*s;

	ft_strcpy(buf, op);
	end[0] = suffix;
	end[1] = '\n';
	end[2] = 0;
	ft_strcat(buf, end);
	if (!ft_strnew(&s, buf) || !ft_arr_append(cmds, &s, 0))
		return (0);
	return (1);
}
