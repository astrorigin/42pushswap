/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/08 22:33:36 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/26 10:46:46 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

static int	proceed_1(t_parms *parms)
{
	t_arr	cmds_in;
	t_arr	cmds_gt;

	if (!init_cmds(&cmds_in, &cmds_gt) || !index_stak(&parms->a))
		return (error(1));
	mark(&parms->a, &mark_by_index);
	if (!solve(parms, &mark_by_index, &cmds_in))
		return (error(1));
	if (!init(&parms->a, parms->argc, parms->argv) || !index_stak(&parms->a))
		return (error(1));
	mark(&parms->a, &mark_by_greater);
	if (!solve(parms, &mark_by_greater, &cmds_gt))
		return (error(1));
	if (cmds_in.nelem <= cmds_gt.nelem)
		print_cmds(&cmds_in);
	else
		print_cmds(&cmds_gt);
	return (1);
}

static int	proceed(t_parms *parms)
{
	if (parms->a.cnt < 2 || stak_is_sorted(&parms->a))
		return (1);
	if (parms->a.cnt == 2)
		sort_2(&parms->a, 'a');
	else if (parms->a.cnt == 3)
		sort_3(&parms->a, 'a');
	else if (parms->a.cnt == 4)
		sort_4(&parms->a, &parms->b, 'a');
	else if (parms->a.cnt == 5)
		sort_5(&parms->a, &parms->b, 'a');
	else if (parms->a.cnt == 6)
		sort_6(&parms->a, &parms->b, 'a');
	else if (parms->a.cnt == 7)
		sort_7(&parms->a, &parms->b, 'a');
	return (proceed_1(parms));
}

int	main(int argc, char *argv[])
{
	t_parms	parms;

	if (argc < 2)
		return (0);
	ft_memset(&parms, 0, sizeof(t_parms));
	parms.argc = argc;
	parms.argv = argv;
	if (!init(&parms.a, argc, argv))
		return (1);
	proceed(&parms);
	ft_memtrash();
	return (0);
}

#ifndef NDEBUG

void	stak_debug(const t_stak *stak)
{
	t_node	*nd;

	ft_printf("===debug===\n");
	nd = stak->top;
	while (nd)
	{
		ft_printf("num=%2d, index=%2d, keep=%d\n", nd->num, nd->index, nd->keep);
		nd = nd->next;
	}
	ft_printf("-----------\n");
	if (stak->cnt)
	{
		ft_printf("top = %2d\n", stak->top->num);
		ft_printf("bottom = %2d\n", stak->bottom->num);
		ft_printf("mark = %2d\n", stak->mark->num);
	}
	else
	{
		ft_printf("top = %p\n", 0);
		ft_printf("bottom = %p\n", 0);
	}
	ft_printf("cnt = %zu\n", stak->cnt);
	ft_printf("keep_cnt = %2zu\n", stak->keep_cnt);
	ft_printf("===========\n");
}

void	shift_debug(const t_shift *sh)
{
	ft_printf("======debug======\n");
	ft_printf("a_nd = %2d\n", sh->a_nd->num);
	ft_printf("b_nd = %2d\n", sh->b_nd->num);
	ft_printf("a_dir = %d\n", sh->a_dir);
	ft_printf("b_dir = %d\n", sh->b_dir);
	ft_printf("sz = %zu\n", sh->sz);
	ft_printf("is_set = %d\n", sh->is_set);
	ft_printf("-----------------\n");
}

#endif
