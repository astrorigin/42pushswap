/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_6.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/15 20:04:04 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/19 23:14:44 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

void	sort_6(t_stak *a, t_stak *b, char which)
{
	const t_node	*nd = stak_find_min(a);

	move_to_top(a, (t_node *) nd, which);
	if (stak_is_sorted(a))
		return ;
	pr_push(a, b, other(which));
	sort_5(a, b, which);
	pr_push(b, a, which);
}
