/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_rotate_both.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/25 23:07:48 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 23:07:59 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

void	do_rotate_both(t_stak *a, t_stak *b)
{
	do_rotate(a);
	do_rotate(b);
}

void	pr_rotate_both(t_stak *a, t_stak *b)
{
	do_rotate_both(a, b);
	print_op("r", 'r');
}

int	add_rotate_both(t_stak *a, t_stak *b, t_arr *cmds)
{
	do_rotate_both(a, b);
	if (!add_op("r", 'r', cmds))
		return (0);
	return (1);
}
