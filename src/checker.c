/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/08 22:33:36 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/28 13:02:37 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

static int	exec(const char *op, t_stak *a, t_stak *b)
{
	if (*op == 's')
		return (exec_s(op, a, b));
	else if (*op == 'p')
		return (exec_p(op, a, b));
	else if (*op == 'r')
		return (exec_r(op, a, b));
	return (0);
}

static int	proceed(t_stak *a, t_stak *b)
{
	char	*op;
	size_t	cnt;

	cnt = 0;
	op = ft_getnextline(0);
	while (op)
	{
		if (!exec(op, a, b))
			return (error(1));
		++cnt;
		ft_free(op);
		op = ft_getnextline(0);
	}
	return (final_check(a, b, cnt));
}

int	main(int argc, char *argv[])
{
	t_stak	a;
	t_stak	b;

	if (argc < 2)
		return (0);
	ft_memset(&a, 0, sizeof(t_stak));
	ft_memset(&b, 0, sizeof(t_stak));
	if (!init(&a, argc, argv))
		return (1);
	proceed(&a, &b);
	ft_memtrash();
	return (0);
}

#ifndef NDEBUG

void	stak_debug(const t_stak *stak)
{
	t_node	*nd;

	ft_printf("===debug===\n");
	nd = stak->top;
	while (nd)
	{
		ft_printf("%d\n", nd->num);
		nd = nd->next;
	}
	ft_printf("---\n");
	if (stak->cnt)
	{
		ft_printf("top = %d\n", stak->top->num);
		ft_printf("bottom = %d\n", stak->bottom->num);
	}
	else
	{
		ft_printf("top = %p\n", 0);
		ft_printf("bottom = %p\n", 0);
	}
	ft_printf("cnt = %zu\n", stak->cnt);
}

#endif
