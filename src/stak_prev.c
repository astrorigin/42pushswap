/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stak_prev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/24 18:32:37 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/24 18:33:45 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

t_node	*stak_prev(const t_stak *stak, const t_node *nd)
{
	t_node	*prev;

	prev = nd->prev;
	if (!prev)
		return (stak->bottom);
	return (prev);
}
