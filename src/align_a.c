/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   align_a.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/25 14:36:26 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 23:00:58 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

static void	_ra(t_stak *a, size_t ra, t_arr *cmds)
{
	size_t	i;

	i = -1;
	while (++i < ra)
		add_rotate(a, 'a', cmds);
}

static void	_rra(t_stak *a, size_t rra, t_arr *cmds)
{
	size_t	i;

	i = -1;
	while (++i < rra)
		add_rev_rotate(a, 'a', cmds);
}

// bring node with index 0 on top of stack a
void	align_a(t_stak *a, t_arr *cmds)
{
	t_node	*nd;
	size_t	ra;
	size_t	rra;

	ra = 0;
	nd = a->top;
	while (nd->index != 0)
	{
		++ra;
		nd = stak_next(a, nd);
	}
	rra = 0;
	nd = a->top;
	while (nd->index != 0)
	{
		++rra;
		nd = stak_prev(a, nd);
	}
	if (ra < rra)
		_ra(a, ra, cmds);
	else
		_rra(a, rra, cmds);
	assert(a->top->index == 0);
}
