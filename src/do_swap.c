/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_swap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/09 18:16:54 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 12:42:32 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

void	do_swap(t_stak *stak)
{
	t_node	*nd;

	if (stak->cnt < 2)
		return ;
	nd = stak->top->next;
	ft_dllist_take(&stak->top, nd, stak->top);
	ft_dllist_push(&stak->top, nd);
	if (stak->cnt == 2)
		stak->bottom = nd->next;
}

void	pr_swap(t_stak *stak, char which)
{
	do_swap(stak);
	print_op("s", which);
}

int	add_swap(t_stak *stak, char which, t_arr *cmds)
{
	do_swap(stak);
	if (!add_op("s", which, cmds))
		return (0);
	return (1);
}

void	do_swap_both(t_stak *a, t_stak *b)
{
	do_swap(a);
	do_swap(b);
}

void	pr_swap_both(t_stak *a, t_stak *b)
{
	do_swap_both(a, b);
	print_op("s", 's');
}
