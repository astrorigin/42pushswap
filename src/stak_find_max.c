/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stak_find_max.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/20 03:25:32 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/20 03:25:46 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

t_node	*stak_find_max(const t_stak *stak)
{
	t_node	*nd;
	t_node	*ret;

	if (!stak->cnt)
		return (0);
	nd = stak->top;
	ret = nd;
	nd = nd->next;
	while (nd)
	{
		if (nd->num > ret->num)
			ret = nd;
		nd = nd->next;
	}
	return (ret);
}
