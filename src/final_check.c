/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   final_check.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/12 14:26:42 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 23:09:08 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

#ifndef NDEBUG

// if OK, print count of operations (for test.py)

int	final_check(const t_stak *a, const t_stak *b, size_t cnt)
{
	if (b->cnt)
		return (0 * ft_putstr("KO\n", 1));
	if (a->cnt < 2 || stak_is_sorted(a))
		return (0 * ft_printf("%zu\n", cnt));
	return (0 * ft_putstr("KO\n", 1));
}

#else

// if OK, print OK

int	final_check(const t_stak *a, const t_stak *b, size_t cnt)
{
	(void) cnt;
	if (b->cnt)
		return (0 * ft_putstr("KO\n", 1));
	if (a->cnt < 2 || stak_is_sorted(a))
		return (0 * ft_putstr("OK\n", 1));
	return (0 * ft_putstr("KO\n", 1));
}

#endif
