/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_a.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/24 10:12:16 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 23:25:53 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

static int	needs_pb(const t_stak *stak)
{
	t_node	*nd;

	nd = stak->top;
	while (nd)
	{
		if (!nd->keep)
			return (1);
		nd = nd->next;
	}
	return (0);
}

static int	needs_sa(t_stak *stak, size_t (*marker)(t_stak *, t_node *))
{
	size_t	keep_cnt;

	if (stak->cnt >= 2)
	{
		do_swap(stak);
		keep_cnt = (*marker)(stak, stak->mark);
		do_swap(stak);
		(*marker)(stak, stak->mark);
		if (keep_cnt > stak->keep_cnt)
			return (1);
	}
	return (0);
}

// move to stack b all nodes with keep=0
int	solve_a(t_parms *parms, size_t (*marker)(t_stak *, t_node *),
		t_arr *cmds)
{
	while (needs_pb(&parms->a))
	{
		if (needs_sa(&parms->a, marker))
		{
			if (!add_swap(&parms->a, 'a', cmds))
				return (0);
			parms->a.keep_cnt = marker(&parms->a, parms->a.mark);
		}
		else if (!parms->a.top->keep)
		{
			if (!add_push(&parms->a, &parms->b, 'b', cmds))
				return (0);
		}
		else
		{
			if (!add_rotate(&parms->a, 'a', cmds))
				return (0);
		}
	}
	return (1);
}
