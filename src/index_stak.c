/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   index_stak.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/23 19:49:40 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/25 23:12:05 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

int	index_stak(t_stak *a)
{
	int		*numbers;
	t_node	*nd;
	size_t	i;

	numbers = ft_calloc(a->cnt, sizeof(int));
	if (!numbers)
		return (0);
	nd = a->top;
	i = 0;
	while (nd)
	{
		numbers[i++] = nd->num;
		nd = nd->next;
	}
	ft_bsort(numbers, a->cnt, sizeof(int), (t_compar) & compar_int);
	i = 0;
	while (i < a->cnt)
	{
		nd = stak_find_num(a, numbers[i]);
		assert(nd);
		nd->index = i++;
	}
	ft_free(numbers);
	return (1);
}
