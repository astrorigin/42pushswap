/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arr_init.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/22 22:49:07 by pmarquis          #+#    #+#             */
/*   Updated: 2022/12/23 15:21:19 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_arr.h"

int	ft_arr_init(t_arr *a, size_t nelem, size_t unit)
{
	assert(unit);
	a->nelem = 0;
	a->unit = unit;
	a->ncapacity = 0;
	a->data = 0;
	if (nelem)
	{
		a->data = ft_calloc(nelem, unit);
		if (!a->data)
			return (0);
		a->ncapacity = nelem;
	}
	return (1);
}
